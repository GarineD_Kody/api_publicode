import Router from '@koa/router'
import Koa from 'koa'
import Engine from 'publicodes'
import { koaMiddleware as publicodesAPI } from '@publicodes/api'


import pkg from 'yaml';
import * as fs from "fs";
const { parse } = pkg;

const app = new Koa()
const router = new Router()

// Create middleware with your Engine

const data = fs.readFileSync('./co2-model.FR-lang.fr-opti.json', 'utf8');

const apiRoutes = publicodesAPI(
    new Engine(
        parse(data)
    )
)

// Basic routes usage (/evaluate, /rules, etc.)
router.use(apiRoutes)

// Or use with specific route prefix (/v1/evaluate, /v1/rules, etc.)
router.use('/v1', apiRoutes)

app.use(router.routes()).listen(3000)